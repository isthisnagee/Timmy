import os
# Using Flask since Python doesn't have built-in session management
from flask import Flask, session, render_template
# Our target library
import requests

app = Flask(__name__)

# Generate a secret random key for the session
app.secret_key = os.urandom(24)

# Define routes for the examples to actually run
@app.route('/run_get')
def run_get():
	url = 'http://httpbin.org/get'

	# the query string is built from this dict. requests replaces "unsafe"
	# characters such as ? or spaces in the string to their safe counterparts,
	# so you can actually put your data in here without worries
	params = {'query': "I'm a query string", 'other': 'Look!?'}

	# this issues a GET to the url. replace "get" with "post", "head",
	# "put", "patch"... to make a request using a different method
	r = requests.get(url, params=params)

	return r.content

@app.route('/run_post')
def run_post():
	url = 'http://httpbin.org/post'

	data = {'website': 'runnable', 'example': 'posting data'}

	# there are two arguments used in this example: "params" and "data".
	# "params" is used to provide data in the query string, while "data"
	# always puts data in the request body. it IS possible to send a get
	# request with a body using the "data" argument
	r = requests.post(url, data=data)

	return r.content

# Define a route for the webserver
@app.route('/')
def index():
	return render_template('index.html')

if __name__ == '__main__':
	app.run(
		host="0.0.0.0",
		port=int("80")
	)
